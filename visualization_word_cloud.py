from colorama import Fore, Style
import matplotlib.pyplot as plt
from wordcloud import WordCloud
from get_words_scores import get_words_scores

words_scores = get_words_scores()

print(f"Rendering the image...", end="", flush=True)
wordcloud = WordCloud(
    background_color="white",
    width=1920,
    height=1080,
    max_font_size=125,
    max_words=399
).generate_from_frequencies(words_scores)
plt.imshow(wordcloud, interpolation='bilinear')
plt.axis("off")
plt.show()
print(f" [{Fore.GREEN}COMPLETE{Style.RESET_ALL}]\n")

image_path = "word_cloud.png"
print(f"Saving the image into \"{image_path}\"...", end="", flush=True)
wordcloud.to_file(image_path)
print(f" [{Fore.GREEN}COMPLETE{Style.RESET_ALL}]\n")

print("Finished! :)")
