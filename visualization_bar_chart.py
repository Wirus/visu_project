from colorama import Fore, Style
import numpy as np
import matplotlib.pyplot as plt
from get_words_scores import get_words_scores

words_scores = get_words_scores()

print("Sorting results in descending order...", end="", flush=True)
sorted_words_scores_r = {k: v for k, v in sorted(words_scores.items(), reverse=True, key=lambda item: item[1])}
sorted_words_scores_r = {f"#{i+1} {w}": sorted_words_scores_r[w] for i, w in enumerate(list(sorted_words_scores_r)[:50])}

sorted_words_scores = {k: v for k, v in sorted(sorted_words_scores_r.items(), key=lambda item: item[1])}
sorted_words_scores_v = list(sorted_words_scores.values())
print(f" [{Fore.GREEN}COMPLETE{Style.RESET_ALL}]\n")

y_pos = np.arange(len(sorted_words_scores.keys()))
plt.figure(figsize=(10,10))
plt.barh(y_pos, sorted_words_scores_v, align='center', alpha=0.5, color=["lightblue", "steelblue"])
plt.yticks(y_pos, sorted_words_scores.keys(), fontsize=8)
plt.ylabel("Words")
plt.xlabel("Scores (occurrences + up-votes + comments)")
plt.tight_layout()

for y, i in enumerate(y_pos):
    value = sorted_words_scores_v[i]
    plt.text(value + 5, y, str(value), color="black", va="center")

print("Rendering the bar chart...", end="", flush=True)
plt.show()
print(f" [{Fore.GREEN}COMPLETE{Style.RESET_ALL}]\n")

image_path = "bar_chart.png"
print(f"Saving the bar chart into \"{image_path}\"...", end="", flush=True)
plt.savefig(image_path)
print(f" [{Fore.GREEN}COMPLETE{Style.RESET_ALL}]\n")
