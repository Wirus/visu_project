import string
import threading
from colorama import Fore, Style
import pandas as pd
import nltk
from wordcloud import STOPWORDS

print("Downloading NLTK's \"stopwords\" and \"punkt\" modules...", end="", flush=True)
nltk.download("stopwords", quiet=True)
nltk.download("punkt", quiet=True)
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize

en_stop_words = set(stopwords.words('english')) | set(STOPWORDS)
en_stop_words = en_stop_words | set(string.punctuation) | set(string.digits)
en_stop_words = en_stop_words | {
    "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
    "00", "10", "11", "12", "13", "14", "15", "16", "17", "18" "19", "20", "30", "40", "50", "60", "70", "80", "90", "100",
    "hundred", "hundreds", "thousand", "thousands", "million", "millions", "billion", "billions",
    "1.0", "1.0.0", "2.0", "2.0.0", "3.0", "4.0", "5.0", "6.0", "7.0", "8.0", "9.0", "10.0",
    "11.0", "12.0", "13.0", "14.0", "15.0", "16.0", "17.0", "18.0", "19.0", "20.0",
    "second", "seconds", "minute", "minutes", "hour", "hours",
    "today", "yesterday", "tomorrow",
    "day", "days", "month", "months", "year", "years",
    "monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday",
    "january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december",
    "2014", "2015", "2016",
    "could", "should", "would", "may", "might", "must", "can", "shall",
    "get", "make", "need", "use", "want", "keep", "let", "run", "take", "launch", "find", "stop", "come", "show", "say",
    "gets", "makes", "needs", "uses", "wants", "keeps", "lets", "goes", "runs", "takes", "launches", "finds", "stops", "comes", "shows", "says",
    "got", "made", "needed", "used", "wanted", "kept", "went", "ran", "took", "launched", "found", "stopped", "came", "showed", "said",
    "getting", "making", "needing", "using", "wanting", "keeping", "letting", "going", "running", "taking", "launching", "finding", "stopping", "coming", "showing", "saying",
    "'m", "n't", "'s", "'ve", "'re",
    "dont", "cant", "wont", "aint",
    "ever", "every", "never", "forever",
    "thing", "things",
    "great", "still", "ago", "always", "many", "well", "good", "bad",
    "please", "pardon", "dear",
    "hn", "yc", "ask"
}
print(f" [{Fore.GREEN}COMPLETE{Style.RESET_ALL}]\n")

df = None
counter = 0
total = 0
words = {}
corrected_words = {
    "US": "U.S.A.",
    "U.S.": "U.S.A.",
    "U.-S.": "U.S.A.",
    "U-S": "U.S.A.",
    "USA": "U.S.A.",
}

def process(start, end):
    global df
    global counter
    global total
    global words
    global corrected_words
    global en_stop_words

    while start < end:
        line = df.iloc[start]
        start += 1
        counter += 1
        print(f"Taking charge of {counter}/{total}...", end=("\r" if counter < total else ""), flush=True)

        word_tokens = word_tokenize(line["title"])
        filtered_sentence = [w.upper() for w in word_tokens if not w.lower() in en_stop_words]

        for word in filtered_sentence:
            if word in corrected_words:
                word = corrected_words[word]
            if word not in words:
                words[word] = 1
            words[word] += line["num_points"] + line["num_comments"]

def get_words_scores():
    global df
    global total
    global words

    print("Reading and parsing the CSV file...", end="", flush=True)
    df = pd.read_csv(
        "data.csv",
        sep=",",
        encoding="utf-8",
        dtype={
            "id": "Int32",
            "title": "string",
            "url": "string",
            "num_points": "Int32",
            "num_comments": "Int32",
            "author": "string",
            "created_at": "string"
        }
    )
    print(f" [{Fore.GREEN}COMPLETE{Style.RESET_ALL}]\n")
    total = len(df)

    threads = []
    n_threads = 20
    div = int(total/n_threads)

    for i in range(n_threads):
        start = div * i
        end = div * (i + 1) if i + 1 != n_threads else total
        print(f"Thread {i+1}/{n_threads}: from {start} to {end}")
        threads.append(threading.Thread(target=process, args=(start, end,)))

    for i in range(n_threads):
        threads[i].start()

    for i in range(n_threads):
        threads[i].join()

    print(f" [{Fore.GREEN}COMPLETE{Style.RESET_ALL}]\n")

    return words
